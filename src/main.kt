import kotlin.properties.Delegates

fun main() {
    /**
     * implementation explicit delegation
     */
    val dataCenter = DataCenter(GetSomeData())
    dataCenter.printSomeData()


    /**
     * implementation implicit delegation
     */
    val rsaKey = RSAKeyPair(RSAPublicKey(), RSASecretKey())
    println("public key : ${rsaKey.rsaPublicKey.key}")
    println("secret key : ${rsaKey.rsaSecretKey.key}")


    /**
     * implementation delegation-properties
     */
    val player = Player()
    println("-> ${player.name}")
    player.name = "Cristiano Renaldo"


    /**
     * lazy() is a function that takes a lambda and returns an instance of Lazy<T> which can serve as a delegate
     * for implementing a lazy property: the first call to get() executes the lambda passed to lazy() and remembers the result,
     * subsequent calls to get() simply return the remembered result.
     */
    val lazyValue: String by lazy {
        println("lazy block now runned")
        "hello world"
    }
    println(lazyValue)
    println(lazyValue)

    /**
     * Delegates.observable() takes two arguments: the initial value and a handler for modifications.
     * The handler gets called every time we assign to the property (after the assignment has been performed).
     * It has three parameters: a property being assigned to, the old value and the new one:
     */
    var memoryUsage: String by Delegates.observable("10%") { property, oldValue, newValue ->
        println("lastUseage : ${oldValue} <-------> current usage : ${newValue}")
    }

    for (i in 5..9) {
        memoryUsage = (i * 5).toString() + "%"
    }


    /**
     * Storing Properties in a Map One common use case is storing the values of properties in a map.
     * This comes up often in applications like parsing JSON or doing other “dynamic” things.
     * In this case, you can use the map instance itself as the delegate for a delegated property
     */
    class FootballPlayer(val map: Map<String, Any?>) {
        val name: String by map
        val age: Int     by map
    }

    val cr7 = FootballPlayer(
        mapOf(
            "name" to "Cristiano Ronaldo",
            "age" to 32
        )
    )

    val leo = FootballPlayer(
        mapOf(
            "name" to "leo messi",
            "age" to 31
        )
    )

    println(cr7.name)
    println(leo.name)


}