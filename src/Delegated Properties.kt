import kotlin.reflect.KProperty

/**
 * simple class for hold player data
 */
class Player {
    var name: String by NameDelegate()
}

/**
 * delegation-properties:
 * class that hold two operator overloading for set and get value
 */
class NameDelegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${property.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to '${property.name}' in $thisRef.")
    }
}