/**
 * simple class
 */
class GetSomeData {

    fun getSomeData(): String {
        return "some data"
    }

}

/**
 * give delegation of  GetSomeData class to DataCenter
 * this is just pass class as constructor example. you can
 * write this sample with inheritance, interface and other language specific
 */
class DataCenter constructor(private val getSomeData: GetSomeData) {

    fun printSomeData() {
        println("${getSomeData.getSomeData()} from ${this::class.java}")
    }

}