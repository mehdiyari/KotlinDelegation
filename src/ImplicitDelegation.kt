/**
 * interface for hold RSA keys : implement by RSAPublic key and RSASecretKey classes
 */
interface RSAKey {
    var key: String
}

/**
 * hold and process RSA public key
 */
class RSAPublicKey : RSAKey {
    override var key: String = "<RSA_PUBLIC_KEY>"

    fun doSomeWork() {
        TODO("do some work with rsa public key")
    }
}

/**
 * hold and process RSA Secret key
 */
class RSASecretKey : RSAKey {
    override var key: String = "<RSA_SECRET_KEY>"

    fun doSomeWork() {
        TODO("do some work with rsa secret key")
    }
}

/**
 * RSA key pair by delegation
 */
data class RSAKeyPair(val rsaPublicKey: RSAKey, val rsaSecretKey: RSAKey) : RSAKey by rsaPublicKey